export default {
  fonts: {
    colors: {
      primary: 'black'
    },
    size: {
      heading: '1.2em',
      button: '1em',
    },
    family: {
      default: ''
    }
  },
  colors: {
    primary: {
      background: '#fee7dc',
      border: '#f7ae6f'
    },
    secondary: {
      background: '#cff1fe',
      border: '#5bb9cf'
    },
    actions: {
      success: '#a0be5a',
      primary: '#eb8439',
    }
  },
  border: {
    width: '1px',
    style: 'solid'
  },
  padding: {
    xsmall: '4px',
    small: '8px',
    medium: '4%'
  },
  margin: {
    xsmall: '8px'
  },
  buttons: {
    primary: {
      background: '#eb8439',
      border: '#fee7dc',
      color: 'white'
    }
  }
}