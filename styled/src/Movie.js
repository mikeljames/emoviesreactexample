import React, {useContext} from 'react';
import {MoviesContext} from './MoviesContext';
import Layout from './components/Layout';
import Button from './components/Button';
import styled from 'styled-components';
const Description = styled.span`
  flex-basis: 60%;
`;
const Poster = styled.img`
  width: 200px;
  height: 300px;
`;
export default ({match, history}) => {
  const movies = useContext(MoviesContext);
  const movie = movies.find(m => m.id === match.params.movieId);
  if (!movie) {
    return <Layout title={'Movie Not Found'}/>
  }
  return <Layout
    title={movie.name}
    style={{flexDirection: 'row'}}
    footer={<Button type="primary" onClick={() => history.goBack()}>Back</Button>}
  >
    <Description>
      {movie.description}
    </Description>
    <Poster src={movie.poster} />
  </Layout>
}