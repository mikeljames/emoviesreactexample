import React, {createContext, useReducer, useState, useEffect} from 'react';
export const CheckoutContext = createContext(null);

const initialState = {};
const reducer = (state, action) => {
  return {
    ...state,
    [action.movie.id]: {
      quantity: action.quantity,
      movie: action.movie
    }
  };
}
export const CheckoutProvider = ({children}) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [total, setTotal] = useState(0.00);
  useEffect(() => {
    const newTotal = Object.entries(state).reduce((acc, [movieId, item]) => {
      return acc+(item.quantity * item.movie.price)
    }, 0);
    setTotal(newTotal.toFixed(2));
  }, [state]);
  return <CheckoutContext.Provider value={
    [state, total, dispatch]
  }>
    {children}
  </CheckoutContext.Provider>
}