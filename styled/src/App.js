import React from 'react';
import EMovies from './EMovies';
import Movie from './Movie';
import {ThemeProvider} from 'styled-components';
import theme from './theme';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import {CheckoutProvider} from './CheckoutContext';
import GlobalStyles from './GlobalStyles';
import {MoviesProvider} from './MoviesContext';
export default () => <>
<GlobalStyles/>
<ThemeProvider theme={theme}>
  <MoviesProvider>
    <CheckoutProvider>
      <Router>
        <Route path="/" exact component={EMovies}/>
        <Route path="/:movieId" exact component={Movie}/>
      </Router>
    </CheckoutProvider>
  </MoviesProvider>
</ThemeProvider>
</>