import React, { useContext } from 'react';
import Layout from './components/Layout';
import MovieItem from './components/MovieItem';
import Total from './components/Total';
import _ from 'lodash';
import { MoviesContext } from './MoviesContext';
import { CheckoutContext } from './CheckoutContext';

function App({history}) {
  const movies = useContext(MoviesContext);
  const [quantities, total, changeQuantity] = useContext(CheckoutContext);
  return (
    <Layout title="Browse movie tickets">
      {movies.map(movie => 
        <MovieItem
          onChangeQuantity={(quantity) => changeQuantity({
            quantity,
            movie
          })}
          onNavigate={() => {
            history.push(`/${movie.id}`)
          }}
          quantity={_.get(quantities, [movie.id, 'quantity'], 0)}
          name={movie.name}
          price={movie.price}
          key={movie.id} />
        )}
      <Total total={total}/>
    </Layout>
  );
}

export default App;
