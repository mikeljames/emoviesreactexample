import {createGlobalStyle} from 'styled-components';
export default createGlobalStyle`
  html, body {
    height: 100%;
    width: 100%;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
  }
  #root {
    display: flex;
    justify-content: space-around;
  }
`;