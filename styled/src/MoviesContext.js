import React from 'react';
const movies = [
  {
    id: '3b21d5a7-d63a-4625-b9e9-8a3f49e6443a',
    name: 'Pulp Fiction',
    description: `Pulp Fiction is a 1994 American[4] crime film written and directed by Quentin Tarantino, who conceived it with Roger Avary.[5] Starring John Travolta, Samuel L. Jackson, Bruce Willis, Tim Roth, Ving Rhames, and Uma Thurman, it tells several stories of criminal Los Angeles. The title refers to the pulp magazines and hardboiled crime novels popular during the mid-20th century, known for their graphic violence and punchy dialogue.

    Tarantino wrote Pulp Fiction in 1992 and 1993, incorporating scenes that Avary originally wrote for True Romance (1993). Its plot occurs out of chronological order. The film is also self-referential from its opening moments, beginning with a title card that gives two dictionary definitions of "pulp". Considerable screen time is devoted to monologues and casual conversations with eclectic dialogue revealing each character's perspectives on several subjects, and the film features an ironic combination of humor and strong violence. TriStar Pictures reportedly turned down the script as "too demented". Miramax co-chairman Harvey Weinstein was enthralled, however, and the film became the first that Miramax fully financed.

    Pulp Fiction won the Palme d'Or at the 1994 Cannes Film Festival, and was a major critical and commercial success. It was nominated for seven awards at the 67th Academy Awards, including Best Picture, and won Best Original Screenplay; it earned Travolta, Jackson, and Thurman Academy Award nominations and boosted their careers. Its development, marketing, distribution, and profitability had a sweeping effect on independent cinema.

    Pulp Fiction is widely regarded as Tarantino's masterpiece, with particular praise for its screenwriting.[6] The self-reflexivity, unconventional structure, and extensive homage and pastiche have led critics to describe it as a touchstone of postmodern film. It is often considered a cultural watershed, influencing movies and other media that adopted elements of its style. In 2008, Entertainment Weekly named it the best film since 1983[7] and it has appeared on many critics' lists of the greatest films ever made. In 2013, Pulp Fiction was selected for preservation in the United States National Film Registry by the Library of Congress as "culturally, historically, or aesthetically significant".[8]`,
    poster: 'http://t2.gstatic.com/images?q=tbn:ANd9GcRz_2nKTNlxhVtzbh29kgL3m2ebLv3TlYyzrbyqBtEUxt6mBuZ-',
    price: 1.99
  },
  {
    id: '4b21d5a7-d63a-4625-b9e9-8a3f49e6443a',
    name: 'America Made',
    description: `American Made is a 2017 American action crime film[4][5] directed by Doug Liman, written by Gary Spinelli, and starring Tom Cruise, Domhnall Gleeson, Sarah Wright, Alejandro Edda, Mauricio Mejía, Caleb Landry Jones, and Jesse Plemons.[6] It is inspired by the life of Barry Seal, a former TWA pilot who flew missions for the CIA, and became a drug smuggler for the Medellín Cartel in the 1980s.[7] In order to avoid jail time, Seal became an informant for the DEA.[7]

    The film was first released in Taiwan on August 18, 2017, and then in the United States on September 29, 2017. It is the first film directed by Liman to be released by Universal Pictures since The Bourne Identity in 2002, and played in 2D and IMAX in select theaters.[8] It grossed $134 million worldwide against a budget of around $50 million and received generally positive reviews from critics, who praised Cruise's performance.`,
    poster: 'https://upload.wikimedia.org/wikipedia/en/c/ca/American_Made_%28film%29.jpg', 
    price: 2.99
  },
]
export const MoviesContext = React.createContext([]);
export const MoviesProvider = ({children}) => {
  return <MoviesContext.Provider value={movies}>
    {children}
  </MoviesContext.Provider>
}