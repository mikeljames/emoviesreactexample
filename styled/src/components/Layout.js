import React from 'react';
import styled from 'styled-components';

const Content = styled.div`
  background-color: ${({theme}) => theme.colors.primary.background};
  border-color: ${({theme}) => theme.colors.primary.border};
  border-width: ${({theme}) => theme.border.width};
  border-style: ${({theme}) => theme.border.style};
  padding: ${({theme}) => theme.padding.medium};
  display: flex;
  flex-direction: column;
  justify-content:space-between;
  margin-top: ${({theme}) => theme.margin.xsmall};
`;

const Wrapper = styled.div`
  width: 80%;
  display: flex;
  align-self: center;
  flex-direction: column;
`;

const Title = styled.span`
  font-size: ${({theme}) => theme.fonts.size.heading};
  font-family: ${({theme}) => theme.fonts.family.default};
  color: ${({theme}) => theme.fonts.colors.primary};
`;

export default ({title, children, footer, style}) => <Wrapper>
  <Title>{title}</Title>
  <Content style={style}>
    {children}
  </Content>
  {footer}
</Wrapper>

