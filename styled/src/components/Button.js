import React from 'react';
import styled from 'styled-components';

export default styled.button`
  border-width: ${({theme}) => theme.border.width};
  border-style: ${({theme}) => theme.border.style};
  border-color: ${({theme, type}) => theme.buttons[type].border};
  background: ${({theme, type}) => theme.buttons[type].background};
  font-size: ${({theme}) => theme.fonts.size.button};
  padding: ${({theme}) => theme.padding.small};
  cursor: pointer;
  color: ${({theme, type}) => theme.buttons[type].color};
`;