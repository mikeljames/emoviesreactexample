import React from 'react';
import styled from 'styled-components';
import QuantityPicker from './QuantityPicker';

const ItemWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: ${({theme}) => theme.margin.xsmall};
`
const ItemName = styled.div`
  background-color: ${({theme}) => theme.colors.secondary.background};
  border-color: ${({theme}) => theme.colors.secondary.border};
  border-width: ${({theme}) => theme.border.width};
  border-style: ${({theme}) => theme.border.style};
  color: ${({theme}) => theme.fonts.colors.primary};
  font-family: ${({theme}) => theme.fonts.family.default};
  padding: ${({theme}) => theme.padding.xsmall};
  flex-basis: 20%;
  text-align: center;
  cursor: pointer;
`;
const ItemPrice = styled.div`
  background-color: ${({theme}) => theme.colors.secondary.background};
  border-color: ${({theme}) => theme.colors.secondary.border};
  border-width: ${({theme}) => theme.border.width};
  border-style: ${({theme}) => theme.border.style};
  color: ${({theme}) => theme.fonts.colors.primary};
  font-family: ${({theme}) => theme.fonts.family.default};
  padding: ${({theme}) => theme.padding.xsmall};
`;

export default ({ name, price, onNavigate, quantity, onChangeQuantity }) => <ItemWrapper>
  <ItemName onClick={onNavigate}>
    {name}
  </ItemName>
  <QuantityPicker value={quantity} onChange={onChangeQuantity}/>
  <ItemPrice>
    {price}
  </ItemPrice>
</ItemWrapper>
