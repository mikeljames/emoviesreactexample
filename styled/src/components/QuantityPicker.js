import React from 'react';
import styled from 'styled-components';

const QuantityPicker = styled.input`
  background-color: ${({theme}) => theme.colors.white};
  border-color: ${({theme}) => theme.colors.secondary.border};
  border-width: ${({theme}) => theme.border.width};
  border-style: ${({theme}) => theme.border.style};
  text-align:center;
`

export default ({value, onChange}) => <QuantityPicker type="number" value={value} onChange={e => {
  onChange(e.target.value)
}}/>