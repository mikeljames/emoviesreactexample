import React from 'react';
import styled from 'styled-components';
const Total = styled.div`
  flex-basis: 100%;
  text-align: right;
  padding: ${({theme}) => theme.padding.medium};
  padding-right: 0;
`;
const TotalValue = styled.span`
  font-family: ${({theme}) => theme.fonts.family.default};
  color: ${({theme}) => theme.fonts.colors.primary};
`
export default ({total}) => <Total>
  <TotalValue>Total £{total}</TotalValue>
</Total>;